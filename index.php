<?php include "init.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>
    <form method="post" action="init.php">
        <div class="buy_ticket">

            <table>
                <thead>
                    <tr>
                        <th>Fullname</th>
                        <th>Movie Name</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Rate</th>
                    </tr>
                    

                    <?php echo '<h1 style="text-align:center;">This week\'s cinema movies</h1>';?>
                    <?php foreach ($get_buyers as $key => $value) :  ?>
                        <tr>
                            <td><?= $value['fullname']; ?></td>
                            <td><?= $value['movie']; ?></td>
                            <td><?= $value['date']; ?></td>
                            <td><?= $value['amount']; ?></td>
                            <td><progress value="<?= $value['rate']; ?>" max="10"></progress></td>
                            <td><input type="number" max="10" value="<?= $value['rate'];?>" name="<?= $value['id']; ?>"</td>
                        </tr>
                    <?php endforeach ?>

                </thead>
            </table>
        </div>
        <div class="add_new">

            <div class="new_list">
                <p class="trick">
                    <input type="submit" class="butt" name="rate" value="Rate" width="10%">
                    <input type="submit" class="butt" name="add" value="Add" width="10%">

                    <p>
            </div>

        </div>
    </form>
</body>

</html>