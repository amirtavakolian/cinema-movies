<?php

function read_db()
{
    $readFile = json_decode(file_get_contents(DB_PATH), 1);
    return $readFile;
}

function list_of_buyers()
{
    $get_buyers = json_decode(file_get_contents(DB_PATH), 1);
    return $get_buyers;
}

function saveRate($new_rate)
{
    $get_db = read_db();
    foreach ($get_db as $key => $value) {
        foreach ($new_rate as $key1 => $value1) {
            if ($key1 == $key) {
                $get_db[$key]['rate'] = $value1;
            }
        }
    }

    saveDb($get_db);
}


function saveDb($new_db)
{
    $get_db = read_db();
    $get_db = $new_db;

    $get_db = json_encode($get_db);
    file_put_contents(DB_PATH, $get_db);
    header('Location: ' . URL);
}

function addMovie($add_movie)
{
    $get_db = read_db();
    $get_db[] = $add_movie;
    $get_db = json_encode($get_db);
    file_put_contents(DB_PATH, $get_db);
    header('Location: ' . URL);
}
